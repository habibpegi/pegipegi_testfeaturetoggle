package com.pegipegi.featuretoggle;

import java.util.HashMap;
import java.util.Map;

import com.pegipegi.featuretoggle.client.SafepitHttpClient;
import com.pegipegi.featuretoggle.sevice.IPaymentService;
import com.pegipegi.featuretoggle.sevice.PaymentService;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
        
        IPaymentService paymentService = new PaymentService(new SafepitHttpClient());
        Map<String, Object> request = new HashMap<String, Object>();
        paymentService.submitPayment(request);
        
        System.out.println( "Bye people!" );
        
    }
}
