package com.pegipegi.featuretoggle.sevice;

import java.util.Map;

public interface IPaymentService {
	public String submitPayment(Map<String, Object> request);
}
