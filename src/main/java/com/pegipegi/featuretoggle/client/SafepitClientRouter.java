package com.pegipegi.featuretoggle.client;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SafepitClientRouter {
	public ISafepitClient getClient() {
		
		//pembacaan properties dilakukan saat startup!
		
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = SafepitClientRouter.class.getClassLoader().getResourceAsStream("toggle.properties");
			prop.load(is);
			
			if("on".equalsIgnoreCase(prop.getProperty("safepit.client.rabbitmq"))) {
				return new SafepitRabbitMQClient();
			}
		} catch (FileNotFoundException e) {
			return new SafepitHttpClient();
		} catch (IOException e) {
			return new SafepitHttpClient();
		}
		
		return new SafepitHttpClient();
	}
}
