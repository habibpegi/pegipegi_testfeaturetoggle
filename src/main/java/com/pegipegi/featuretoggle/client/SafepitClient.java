package com.pegipegi.featuretoggle.client;

import java.util.Map;

public class SafepitClient {
	
	private ISafepitClient client;
	
	public SafepitClient(ISafepitClient client) {
		this.client = client;
	}

	public String sentRequest(Map<String, Object> request) {
		return client.sentRequest(request);
	}

}
