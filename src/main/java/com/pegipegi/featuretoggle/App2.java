package com.pegipegi.featuretoggle;

import java.util.HashMap;
import java.util.Map;

import com.pegipegi.featuretoggle.client.SafepitClientRouter;
import com.pegipegi.featuretoggle.sevice.IPaymentService;
import com.pegipegi.featuretoggle.sevice.PaymentService2;

/**
 * Hello world!
 *
 */
public class App2 {
    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
        
        //new SafepitClientRouter().getClient() harus dilakukan saat startup!
        IPaymentService paymentService = new PaymentService2(new SafepitClientRouter().getClient());
        Map<String, Object> request = new HashMap<String, Object>();
        paymentService.submitPayment(request);
        
        System.out.println( "Bye people!" );
        
    }
}
